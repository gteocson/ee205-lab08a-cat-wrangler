///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// Doubly linked list functions
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04-13-2021
///////////////////////////////////////////////////////////////////////////////

#include "list.hpp"
#include "node.hpp"
#include <cstdlib>
#include <cassert>
#include <iostream>

using namespace std;



const bool DoubleLinkedList::empty() const{

   if(head == nullptr)
      return true;
   else
      return false;

}


void DoubleLinkedList::push_front( Node* newNode ){

   if( newNode == nullptr)
      return;

   if(head != nullptr){
   
      newNode -> next = head;
   
      newNode -> prev = nullptr;
   
      head->prev =  newNode;
   
      head = newNode;
   
   } else{

      newNode -> next = nullptr;
      
      newNode -> prev = nullptr;
      
      head = newNode;
      
      tail = newNode;

   }

   count++;

}

Node* DoubleLinkedList::pop_front(){

   
   Node* temp;
   
   if(head == nullptr)
      
      return nullptr;
   
   else{
      
      temp = head;
      
      head=head->next;
      
      head->prev = nullptr;      //Added
      
      count--;

      return temp;
   }

}

Node* DoubleLinkedList::get_first() const{

   return head;


}

Node* DoubleLinkedList::get_next( const Node* currentNode ) const{


   return currentNode->next;

}

void DoubleLinkedList::push_back( Node* newNode ){

   if( newNode == nullptr )
      return;

   if( tail != nullptr ){
      
      newNode->next = nullptr;
   
      newNode->prev = tail;
      
      tail->next = newNode;
      
      tail = newNode;
   
   }  else{

      newNode -> next = nullptr;

      newNode -> prev = nullptr;

      head = newNode;

      tail = newNode;

   }

   count++;

}

Node* DoubleLinkedList::pop_back() {

   Node* PoppedNode;
   
   if(tail == nullptr)
      
      return nullptr;
   
   else{
      
      PoppedNode = tail;
      
      tail=tail->prev;
      
      tail->next = nullptr;      //Added

      count --;

      return PoppedNode;
   }

}

Node* DoubleLinkedList::get_last() const{

   return tail;

}

Node* DoubleLinkedList::get_prev( const Node* currentNode ) const{

   return currentNode->prev;

}


void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode ){

   if(currentNode == tail){
         tail = newNode;
         newNode->next = currentNode -> next;
         newNode->prev = currentNode;
         currentNode->next = newNode;
   
   }


   else if(head == nullptr){
      newNode -> prev = nullptr;
      newNode -> next = nullptr;
      tail = newNode;
      head = newNode;
   }
   else{

         currentNode -> next -> prev = newNode;
         newNode->next = currentNode -> next;
         newNode->prev = currentNode;
         currentNode->next = newNode;
         
   }

   count++;
}

void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode){
   
   if(currentNode == head){
         head = newNode;
         newNode -> prev = currentNode -> prev;
         newNode -> next = currentNode;
         currentNode -> prev = newNode;
   }


   else if(head == nullptr){
      newNode -> prev = nullptr;
      newNode -> next = nullptr;
      tail = newNode;
      head = newNode; 
   } else{
      
         currentNode -> prev -> next = newNode;
         newNode -> prev = currentNode -> prev;
         newNode -> next = currentNode;
         currentNode -> prev = newNode;
   }
      
   count++;
}



void DoubleLinkedList::swap(Node* node1, Node* node2){

   //10000000000% a way to make this a LOT smaller
    Node* node1next = node1 -> next;
    Node* node1prev = node1 -> prev;
    Node* node2next = node2 -> next;
    Node* node2prev = node2 -> prev;

   if( node1 == node2 )
      return;

   assert(node1 != nullptr);
   assert(node2 != nullptr);

   if( node1 == head && node2 == tail){

      if( node1->next != node2 ){            //If they are not adjacent

         head = node2;                       //Swap head and tail
         tail = node1;

         node2 -> prev -> next = node1;      //Update sides of node2 to now point to node1

         node1 -> next -> prev = node2;      //Update sides of node1 to now point to node2

         node1 -> prev = node2prev;          //Update left sides of nodes
         node2 -> prev = nullptr;

         node2 -> next = node1next;          //Update right side of nodes
         node1 -> next = nullptr;

      }else{
        
         head = node2;
         tail = node1;

         node1 -> prev = node2;
         node2 -> prev = nullptr;

         node1 -> next = nullptr;
         node2 -> next = node1;

      }

      return;

   }


   if( node1 == head && node2 != tail ){

      if(node1->next != node2){              //If they are not adjacent

         head = node2;                       //Set head

         node2 -> prev -> next = node1;      //Update sides of node2
         node2 -> next -> prev = node1;

         node1 -> next -> prev = node2;      //Update sides of node1

         node1 -> prev = node2prev;          //Update left sides
         node2 -> prev = nullptr;

         node1 -> next = node2next;          //Update right sides
         node2 -> next = node1next;

      }else{

         head = node2;                       //Set head

         node2 -> next -> prev = node1;      //Update sides of node2

         node1 -> prev = node2;              //Update left side
         node2 -> prev = nullptr;

         node1 -> next = node2next;
         node2 -> next = node1;              //Update right sides

      }

      return;
   }


   if( node1 != head && node2 == tail ){

      if(node1->next != node2){

         tail = node1;                       //Set tail

         node1 -> prev -> next = node2;      //Update sides of node1
         node1 -> next -> prev = node2;      

         node2 -> prev -> next = node1;      //Update sides of node2


         node2 -> prev = node1prev;          //Update left sides
         node1 -> prev = node2prev;

         
         node2 -> next = node1next;          //Update right sides
         node1 -> next = nullptr;

      }else{

         tail = node1;                       //Set tail

         node1 -> prev -> next = node2;      //Update sides of node 1

         node2 -> prev = node1prev;          //Update left sides
         node1 -> prev = node2;

         node2 -> next = node1;              //Update right sides
         node1 -> next = nullptr;
      }

      return;

   }

   if( node1 != head && node2 != tail){
   
      if( node1->next != node2){             //Used a similar idea to previous but not same
     
         Node* temp = node1;                 //Store to be set

         node2 -> next -> prev = node1;      //Update right side of node 2
         node2 -> next = node1next;

         node2 -> prev -> next = node1;      //Update left side of node 2
         node2 -> prev = node1prev;

         node1 -> next -> prev = node2;      //Update right side of node 1
         node1 -> next = node2next;

         node1 -> prev -> next = node2;      //Update left side of node 1
         node1 -> prev = node2prev;
         
         node1 = node2;                      //Set nodes
         node2 = temp;
     
            
      }else{
   
         Node* temp = node1;                 //Store

         node2 -> next -> prev = node1;      //Update side nodes
         node1 -> prev -> next = node2;
         
         node1 -> next = node2next;          //Update right
         node2 -> next = node1;

         node2 -> prev = node1prev;          //Update left
         node1 -> prev = node2;

         node1 = node2;                      //Set nodes
         node2 = temp;
      }

      return;
   }
}


const bool DoubleLinkedList::isSorted() const {
   if( count <= 1 )                                            //If the size is shorter than 1
      return true;

   for( Node* i = head ; i->next != nullptr ; i = i-> next ){  //Return false if not sorted
      if( *i > *i -> next )
         return false;

   }

   return true;                                                //Otherwise return true
}

void DoubleLinkedList::insertionSort(){

   if( count <= 1)                                             //If list is basically empty just return
      return;


   for( Node* i = head ; i->next != nullptr ; i = i-> next ){  //First iteration of the list ( main list )

      Node* Swappable = i;                                     //Store a node that you want to be the min                                   
      for( Node* j = i->next ; j != nullptr; j=j->next){       //Second iteration of the list to compare

         if( *Swappable > *j )                                 //If anything smaller than swappable make that swappable
            Swappable = j;

      }

      swap(i, Swappable );                                     //Swap the first i with the smallest node
      i=Swappable;                                             //Set the i to the smallest current number

   }                                                           //Find the next node to i

}



