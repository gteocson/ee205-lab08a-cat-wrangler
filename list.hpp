///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// Header file for list
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04-13-2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

using namespace std;


class DoubleLinkedList{

public:

   Node* next;

   Node* prev;

   unsigned int count;

   //Prev Funcs

   const bool empty() const;


   void push_front( Node* newNode );


   Node* pop_front();


   Node* get_first() const;


   Node* get_next( const Node* currentNode ) const;

   inline unsigned int size() const{return count; };

   //New Funcs

   void push_back( Node* newNode );

   Node* pop_back();

   Node* get_last() const;

   Node* get_prev( const Node* currentNode ) const;

   void swap(Node* node1, Node* node2);

   const bool isSorted() const; 

   void insertionSort();

   //EC Funcs
   
   void insert_after( Node* currentNode, Node* newNode );

   void insert_before( Node* currentNode, Node* newNode );



protected:

   Node* head = nullptr;

   Node* tail = nullptr;

};


